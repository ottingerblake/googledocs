(function() {
    var css = "::-webkit-scrollbar { -webkit-appearance: none; width: 0px; }";
    
    var node = document.createElement("style");
    node.type = "text/css";
    node.appendChild(document.createTextNode(css));
    var heads = document.getElementsByTagName("head");
    if (heads.length > 0) {
        heads[0].appendChild(node); 
    } else {
        // no head yet, stick it whereever
        document.documentElement.appendChild(node);
    }

})();

// Attribution: Implemented based on https://github.com/ivoxavier/Tweads/blob/master/qml/scrollBarTheme.js